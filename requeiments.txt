fastapi == 0.75.1
uvicorn == 0.17.6
python-multipart == 0.0.5
pyjwt==2.3.0
python-jose[cryptography] == 3.3.0
passlib[bcrypt] == 1.7.4
SQLAlchemy==1.4.35
psycopg2 == 2.9.3
alembic ==1.7.7